resource "aws_security_group" "this" {
  name   = "${var.app_name}-${var.container_name}-sg"
  vpc_id = data.aws_vpc.selected.id

  # TODO This should be more restricted
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    description = "internet"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
  }

  tags = {
    Name    = "${var.app_name}-${var.container_name}-sg"
    Product = var.app_name
  }
}
